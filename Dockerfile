FROM alpine:3.15

RUN apk add --no-cache ca-certificates

COPY ./webhook /usr/local/bin/webhook

ENTRYPOINT ["/usr/local/bin/webhook"]
